<?php

/**
 * @file
 * uw_ct_daily_menu.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_daily_menu_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_daily_menu_legend';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'uw_fs_view_daily_menu' => 'uw_fs_view_daily_menu',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-558d0d774f41d07721d646509e145104' => array(
          'module' => 'views',
          'delta' => '558d0d774f41d07721d646509e145104',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['uw_daily_menu_legend'] = $context;

  return $export;
}
