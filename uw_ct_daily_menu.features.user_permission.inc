<?php

/**
 * @file
 * uw_ct_daily_menu.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_daily_menu_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer daily menu settings'.
  $permissions['administer daily menu settings'] = array(
    'name' => 'administer daily menu settings',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_food_services',
  );

  // Exported permission: 'create uw_ct_daily_menu content'.
  $permissions['create uw_ct_daily_menu content'] = array(
    'name' => 'create uw_ct_daily_menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in uw_fs_dietary_types'.
  $permissions['define view for terms in uw_fs_dietary_types'] = array(
    'name' => 'define view for terms in uw_fs_dietary_types',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary uw_fs_dietary_types'.
  $permissions['define view for vocabulary uw_fs_dietary_types'] = array(
    'name' => 'define view for vocabulary uw_fs_dietary_types',
    'roles' => array(),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any uw_ct_daily_menu content'.
  $permissions['delete any uw_ct_daily_menu content'] = array(
    'name' => 'delete any uw_ct_daily_menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_daily_menu content'.
  $permissions['delete own uw_ct_daily_menu content'] = array(
    'name' => 'delete own uw_ct_daily_menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in uw_fs_dietary_types'.
  $permissions['delete terms in uw_fs_dietary_types'] = array(
    'name' => 'delete terms in uw_fs_dietary_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_fs_meal_type'.
  $permissions['delete terms in uw_fs_meal_type'] = array(
    'name' => 'delete terms in uw_fs_meal_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_daily_menu content'.
  $permissions['edit any uw_ct_daily_menu content'] = array(
    'name' => 'edit any uw_ct_daily_menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_daily_menu content'.
  $permissions['edit own uw_ct_daily_menu content'] = array(
    'name' => 'edit own uw_ct_daily_menu content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in uw_fs_dietary_types'.
  $permissions['edit terms in uw_fs_dietary_types'] = array(
    'name' => 'edit terms in uw_fs_dietary_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_fs_meal_type'.
  $permissions['edit terms in uw_fs_meal_type'] = array(
    'name' => 'edit terms in uw_fs_meal_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_daily_menu revision log entry'.
  $permissions['enter uw_ct_daily_menu revision log entry'] = array(
    'name' => 'enter uw_ct_daily_menu revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'merge uw_fs_dietary_types terms'.
  $permissions['merge uw_fs_dietary_types terms'] = array(
    'name' => 'merge uw_fs_dietary_types terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'merge uw_fs_meal_type terms'.
  $permissions['merge uw_fs_meal_type terms'] = array(
    'name' => 'merge uw_fs_meal_type terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'override uw_ct_daily_menu authored by option'.
  $permissions['override uw_ct_daily_menu authored by option'] = array(
    'name' => 'override uw_ct_daily_menu authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_daily_menu authored on option'.
  $permissions['override uw_ct_daily_menu authored on option'] = array(
    'name' => 'override uw_ct_daily_menu authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_daily_menu comment setting option'.
  $permissions['override uw_ct_daily_menu comment setting option'] = array(
    'name' => 'override uw_ct_daily_menu comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_daily_menu promote to front page option'.
  $permissions['override uw_ct_daily_menu promote to front page option'] = array(
    'name' => 'override uw_ct_daily_menu promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_daily_menu published option'.
  $permissions['override uw_ct_daily_menu published option'] = array(
    'name' => 'override uw_ct_daily_menu published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_daily_menu revision option'.
  $permissions['override uw_ct_daily_menu revision option'] = array(
    'name' => 'override uw_ct_daily_menu revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_daily_menu sticky option'.
  $permissions['override uw_ct_daily_menu sticky option'] = array(
    'name' => 'override uw_ct_daily_menu sticky option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  return $permissions;
}
