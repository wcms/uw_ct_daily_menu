<?php

/**
 * @file
 * uw_ct_daily_menu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_daily_menu_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_daily_menu_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_daily_menu_image_default_styles() {
  $styles = array();

  // Exported image style: uw_fs_dm_diet_icon_style.
  $styles['uw_fs_dm_diet_icon_style'] = array(
    'label' => 'Food services dietary icon',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 20,
          'height' => 20,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_daily_menu_node_info() {
  $items = array(
    'uw_ct_daily_menu' => array(
      'name' => t('Daily menus'),
      'base' => 'node_content',
      'description' => t('Content type for daily menus.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_daily_menu_paragraphs_info() {
  $items = array(
    'uw_fs_dm_daily_outlet_menu' => array(
      'name' => 'Daily outlet menu',
      'bundle' => 'uw_fs_dm_daily_outlet_menu',
      'locked' => '1',
    ),
    'uw_fs_para_daily_menu' => array(
      'name' => 'Daily menu',
      'bundle' => 'uw_fs_para_daily_menu',
      'locked' => '1',
    ),
  );
  return $items;
}
