<?php

/**
 * @file
 * uw_ct_daily_menu.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_daily_menu_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_meal|paragraphs_item|uw_fs_dm_daily_outlet_menu|default';
  $field_group->group_name = 'group_meal';
  $field_group->entity_type = 'paragraphs_item';
  $field_group->bundle = 'uw_fs_dm_daily_outlet_menu';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => '',
    'weight' => '0',
    'children' => array(
      0 => 'field_uw_fs_dm_meal_type',
      1 => 'field_uw_fs_dm_menu_items',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-meal field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_meal|paragraphs_item|uw_fs_dm_daily_outlet_menu|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_fs_dm_footer|node|uw_ct_daily_menu|form';
  $field_group->group_name = 'group_uw_fs_dm_footer';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_ct_daily_menu';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Footer',
    'weight' => '4',
    'children' => array(
      0 => 'field_uw_fs_dm_footer_info',
      1 => 'field_uw_fs_dm_footer_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uw-fs-dm-footer field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uw_fs_dm_footer|node|uw_ct_daily_menu|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Footer');

  return $field_groups;
}
