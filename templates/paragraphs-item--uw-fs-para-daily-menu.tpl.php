<?php

/**
 * @file
 * Template file for location with daily menu (ie. The Market, Mudies, etc.)
 *
 * Drupal path: locations-and-hours/daily-menu.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<article class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <h3 class="dm-location"><?php
      $location_name = $content['field_uw_fs_dm_location'];
      foreach ($location_name as $key => $outlet) {
        if (is_int($key)) {
          print render($outlet);
        }
      }
    ?></h3>
    <div class="dm-whole-outlet"><?php print render($content['field_uw_fs_dm_meal_types']) ?></div>
  </div>
</article>
