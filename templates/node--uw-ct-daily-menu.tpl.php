<?php

/**
 * @file
 * Template for page content at path locations-and-hours/daily-menu.
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php if ($display_submitted): ?>
      <div class="submitted"><?php print $date; ?> — <?php print $name; ?></div>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_uw_fs_dm_date']);

      // Rendering everything except footer title & information for styling of those two components.
        $ignore = ['field_uw_fs_dm_footer_title', 'field_uw_fs_dm_footer_info'];
        foreach ($content as $key => $value) {
          if (!in_array($key, $ignore)) {
            print render($content[$key]);
          }
        }
      ?>
      <br><hr>
      <div class="dm-footer-title">
        <?php print render($content['field_uw_fs_dm_footer_title']); ?>
      </div>
      <br>
      <div>
        <?php print render($content['field_uw_fs_dm_footer_info']); ?>
      </div>

    </div>

    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>

  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
