<?php

/**
 * @file
 * Template file for Menu type (ie. Hot table, Lunch, Sandwich etc) and the menu items under each category.
 *
 * Drupal path: locations-and-hours/daily-menu.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */
?>
<section class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <h4 class="dm-menu-type"><?php print $content['group_meal']['field_uw_fs_dm_meal_type'][0]['#markup']; ?></h4>
    <ul class="dm-menus">
      <?php
      $menu_items_array = $content['group_meal']['field_uw_fs_dm_menu_items'];
      foreach ($menu_items_array as $key => $item_array) {
        if (is_int($key)) {
          ?>
          <li class="dm-menu-item">
            <?php
            $diet_type = $item_array['#item']['entity']->field_uw_ct_food_diet_type;
            if (!empty($diet_type)) {
              $icon_arr = $diet_type['und'];
              foreach ($icon_arr as $icon_sub_arr) {
                $tax_id = $icon_sub_arr['tid'];
                if (isset($dietary_icons[$tax_id])) {
                  $icon = $dietary_icons[$tax_id];
                  if ($icon) {
                    $variables = array(
                      'path' => $icon['uri'],
                      'style_name' => 'uw_fs_dm_diet_icon_style',
                      'title' => $icon['title'],
                    );
                    print theme('image_style', $variables);
                  }
                }
              }
            }
            print render($item_array);
            ?>
          </li>
          <?php
        }
      }
      ?>
    </ul>
  </div>
</section>
